function each(inventoryObject, callBackFunction){
    let array = [];
    for (let number in inventoryObject){
        array.push(callBackFunction(inventoryObject[number])); 
        //console.log(number);
    }
    return array;
}

module.exports = each;