function filter(inventoryObject, callBackFunction){

    let array = [];

    for (let index=0; index<inventoryObject.length; index++){
        let returnValue = callBackFunction(inventoryObject[index], index, inventoryObject);
        if (returnValue == true){
            array.push(inventoryObject[index])
        }
    }
    return array;
}

module.exports = filter;