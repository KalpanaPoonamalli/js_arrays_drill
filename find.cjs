function find(inventoryObject, callBackFunction){
    let array = [];
    for (let number in inventoryObject){
        let returnValue = callBackFunction(inventoryObject[number])
        if (returnValue){
            array.push(inventoryObject[number])
        }
    }
    return array;
}

module.exports = find;