function flatten(nestedArray, depth=1){

    let array = [];

    if (nestedArray.length == 0){
        return [];
    }
    
    for (let index = 0; index < nestedArray.length; index++){
        let element = nestedArray[index];
            if(Array.isArray(element) && depth >= 1){
                array = array.concat(flatten(element, depth - 1));
            }else {
                if (element == undefined || element == null){
                    continue;
                }else {
                    array.push(element);
                }
            }
        }

    return array
}

module.exports = flatten;