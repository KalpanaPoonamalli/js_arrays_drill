function reduce(inventoryObject, callBackFunction, startingValue){
    
    let accumulator;

    if ( startingValue == undefined){
        accumulator = inventoryObject[0]
    }else{
        accumulator = startingValue
    }    

    //console.log(sum)

    for (let index = startingValue !== undefined ? 0 : 1; index<inventoryObject.length; index++){
        accumulator = (callBackFunction(accumulator, inventoryObject[index], index, inventoryObject)); 
    }
    return accumulator;
}

module.exports = reduce;