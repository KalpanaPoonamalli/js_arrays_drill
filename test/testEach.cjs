let inventoryObject = require('../inventoryArray.cjs');
let each = require('../each.cjs');

function callBackFunction(number){
    return number * number
}

const result = each(inventoryObject, callBackFunction);

console.log(result);