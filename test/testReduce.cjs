let inventoryObject = require('../inventoryArray.cjs');
let reduce = require('../reduce.cjs');


function callBackFunction(accumulator, number, index, inventoryObject){
    return accumulator + number;
};

let startingValue;

const result = reduce(inventoryObject, callBackFunction, startingValue);

console.log(result);